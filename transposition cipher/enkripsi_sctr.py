import pyperclip

def main():
    pesanku = 'tanggal 26 adalah tanggal UAS kita mulai.'
    kunciku = 8
    rahasia = enkripsi(kunciku, pesanku)
    print(rahasia + '|')

    pyperclip.copy(rahasia)

def enkripsi(kunci, pesan):
    rahasia = [''] * kunci
    for uas in range(kunci):
        petunjuk = uas
        while petunjuk < len(pesan):
            rahasia[uas] += pesan[petunjuk]
            petunjuk += kunci
    return ''.join(rahasia)
if __name__ == '__main__':
    main()
